package ru.crabdance.userfront.dao;

import org.springframework.data.repository.CrudRepository;
import ru.crabdance.userfront.domain.Appointment;

import java.util.List;
import java.util.Optional;

public interface AppointmentDao extends CrudRepository<Appointment, Long> {
    List<Appointment> findAll();
    Optional<Appointment> findById(Long id);
}

