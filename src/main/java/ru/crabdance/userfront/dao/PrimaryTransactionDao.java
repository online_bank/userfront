package ru.crabdance.userfront.dao;

import org.springframework.data.repository.CrudRepository;
import ru.crabdance.userfront.domain.PrimaryTransaction;

import java.util.List;

public interface PrimaryTransactionDao extends CrudRepository<PrimaryTransaction, Long> {
    List<PrimaryTransaction> findAll();
}
