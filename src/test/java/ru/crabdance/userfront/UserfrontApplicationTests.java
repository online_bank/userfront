package ru.crabdance.userfront;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserfrontApplicationTests {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void openRootAndRedirectToIndex() throws Exception {
        mockMvc.perform(get("/"))
                .andDo(print())
                .andExpect(redirectedUrl("/index"));
    }


    @Test
    public void shoulIndexStatus200() throws Exception {
        mockMvc.perform(get("/index"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void shoulSignUpStatus200() throws Exception {
        mockMvc.perform(get("/signup"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void signUpCreateUser() throws Exception {
        mockMvc.perform(post("/signup")
                .param("firstName", "joe")
                .param("lastName", "black")
                .param("phone", "1234567890")
                .param("email", "joe@black.com")
                .param("username", "joeblack")
                .param("password", "123"))
                .andDo(print())
                //.andExpect(status().isOk())
                .andExpect(redirectedUrl("/"));
    }
}
