package ru.crabdance.userfront.dao;

import org.springframework.data.repository.CrudRepository;
import ru.crabdance.userfront.domain.security.Role;

public interface RoleDao extends CrudRepository<Role, Integer> {
    Role findByName(String name);
}
