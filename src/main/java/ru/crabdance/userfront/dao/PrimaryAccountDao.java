package ru.crabdance.userfront.dao;

import org.springframework.data.repository.CrudRepository;
import ru.crabdance.userfront.domain.PrimaryAccount;

public interface PrimaryAccountDao extends CrudRepository<PrimaryAccount, Long> {
    PrimaryAccount findByAccountNumber (int accountNumber);
}
