package ru.crabdance.userfront.dao;

import org.springframework.data.repository.CrudRepository;
import ru.crabdance.userfront.domain.User;

public interface UserDao  extends CrudRepository<User, Long> {
     User findByUsername(String username);
     User findByEmail(String email);
}
