package ru.crabdance.userfront.service;

import java.security.Principal;

import ru.crabdance.userfront.domain.PrimaryAccount;
import ru.crabdance.userfront.domain.PrimaryTransaction;
import ru.crabdance.userfront.domain.SavingsAccount;
import ru.crabdance.userfront.domain.SavingsTransaction;


public interface AccountService {
	PrimaryAccount createPrimaryAccount();
    SavingsAccount createSavingsAccount();
    void deposit(String accountType, double amount, Principal principal);
    void withdraw(String accountType, double amount, Principal principal);
}
